<?php
function denied() {
    http_response_code(403);
    exit(403);
}

if (!isset($_GET['token'])) denied();
if ($_GET['token'] !== 'wawa') denied();

shell_exec('git pull >> log.txt 2>&1 &');

$json = file_get_contents('php://input');
$json = json_decode($json);
if (property_exists($json, 'push')) {
    $versi = $json->push->changes[0]->commits[0]->hash;
    $versi = substr($versi, 0, 6);
    $file = fopen('versi.txt', 'w');
    fwrite($file, $versi);
    fclose($file);
}

echo "Git pulling...";